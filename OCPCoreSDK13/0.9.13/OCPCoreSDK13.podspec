Pod::Spec.new do |s|
	s.name = 'OCPCoreSDK13'
	s.version = '0.9.13'
	s.swift_version = '5'
	s.summary = 'Core Components for Odyssey Computing Platform, and only for iOS 14 or newer'
	s.homepage = 'https://bitbucket.org/odysseyinc/ocpcoresdk_ios13'
    	s.license = { :type => 'Custom', :file => 'LICENSE.md' }
	s.platform = :ios, '15.0'

	s.author = 'Osamu Chiba, Karim Alami, et al'

	s.source = { :git => 'https://bitbucket.org/odysseyinc/ocpcoresdk_ios13.git', :tag => s.version }
	s.source_files = 'OCPCoreSDK13/**/*.{h,m,swift}'

	s.frameworks = 'CoreData'
	s.dependency 'OCPCoreSDK'
	s.requires_arc = true
	s.resource_bundles = { 'OCPCoreSDK13' => 'OCPCoreSDK13/Resources/*' }
end
