Pod::Spec.new do |s|
	s.name = 'OCPMediaSDK'
	s.version = '0.9.6'
    
	s.swift_version = '5.0'
	s.summary = 'Media Components for Odyssey Computing Platform'
	s.homepage = 'https://bitbucket.org/odysseyinc/ocpmediasdk_ios'
    s.license = { :type => 'Custom', :file => 'LICENSE.md' }
	s.platform = :ios, '11.0'

	s.author = 'Mark Pospesel, Karim Alami, et al'

	s.source = { :git => 'https://bitbucket.org/odysseyinc/ocpmediasdk_ios.git', :tag => s.version }
	s.source_files = 'OCPMediaSDK/**/*.{h,m,swift}'

    s.static_framework = true
    s.frameworks = 'Photos'
    s.dependency 'DZNEmptyDataSet'
	s.dependency 'AZSClient'
    s.dependency 'OCPCoreSDK'
	s.requires_arc = true
    s.resource_bundles = { 'OCPMediaSDK' => 'OCPMediaSDK/Resources/*' }
end
