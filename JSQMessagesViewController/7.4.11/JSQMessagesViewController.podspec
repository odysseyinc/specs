Pod::Spec.new do |s|
	s.name = 'JSQMessagesViewController'
	s.version = '7.4.11'
	s.summary = 'An elegant messages UI library for iOS.'
	s.homepage = 'https://bitbucket.org/odysseyinc/odcmessagesviewcontroller'
    s.license = 'MIT'
	s.platform = :ios, '10.0'

	s.author = 'Jesse Squires'

	s.source = { :git => 'https://bitbucket.org/odysseyinc/odcmessagesviewcontroller.git', :tag => s.version }
	s.source_files = 'JSQMessagesViewController/**/*.{h,m}'

	s.resources = ['JSQMessagesViewController/Assets/JSQMessagesAssets.bundle', 'JSQMessagesViewController/**/*.{xib}']

	s.frameworks = 'QuartzCore', 'CoreGraphics', 'CoreLocation', 'MapKit', 'MobileCoreServices', 'AVFoundation'
	s.requires_arc = true
end
