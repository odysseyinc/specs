Pod::Spec.new do |s|
	s.name = 'OCPCoreSDK'
	s.version = '1.1.25'
	s.swift_version = '5.0'
	s.summary = 'Core Components for Odyssey Computing Platform'
	s.homepage = 'https://bitbucket.org/odysseyinc/ocpcoresdk_ios'
    	s.license = { :type => 'Custom', :file => 'LICENSE.md' }
	s.platform = :ios, '11.0'

	s.author = 'Mark Pospesel, Karim Alami, et al'

	s.source = { :git => 'https://bitbucket.org/odysseyinc/ocpcoresdk_ios.git', :tag => s.version }
	s.source_files = 'OCPCoreSDK/**/*.{h,m,swift}'

	s.frameworks = 'CoreData'
	s.dependency 'CocoaLumberjack/Swift'
	s.dependency 'DataCompression'
    s.dependency 'KeychainAccess'
    s.dependency 'libPhoneNumber-iOS'
	s.requires_arc = true
	s.resource_bundles = { 'OCPCoreSDK' => 'OCPCoreSDK/Resources/*' }
end
