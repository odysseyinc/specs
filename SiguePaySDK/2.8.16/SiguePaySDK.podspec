Pod::Spec.new do |s|
	s.name = 'SiguePaySDK'
	s.version = '2.8.16'
    
	s.swift_version = '5.0'
	s.summary = 'Payment functionality powered by SiguePay'
	s.homepage = 'https://bitbucket.org/odysseyinc/siguepaysdk_ios'
    s.license = 'Custom'
	s.platform = :ios, '10.0'

	s.author = 'Mark Pospesel, Karim Alami, et al'

	s.source = { :git => 'https://bitbucket.org/odysseyinc/siguepaysdk_ios.git', :tag => s.version }
	s.source_files = 'SiguePaySDK/**/*.{h,m,swift}'

    s.static_framework = true
	s.frameworks = 'CoreData'
	s.dependency 'OCPCoreSDK'
    s.dependency 'GooglePlaces'
    s.dependency 'SwipeCellKit'
	s.requires_arc = true
    s.resource_bundles = { 'SiguePaySDK' => 'SiguePaySDK/Resources/*' }
    s.resources = 'SiguePaySDK/Model/PaymentsDataModel.xcdatamodeld'
end
